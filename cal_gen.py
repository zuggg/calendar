import calendar
from datetime import timedelta, date

###############################################################################
# Variables

start_year = 2022
start_month = 9
months = 1
output = 'output.tex'

print_week_planner = True
print_monthly_agenda = True
print_weekly_agenda = True

###############################################################################
# Useful functions

# d1, d2 = first_and_last_days(2022, 9, 5)
# ((d2-d1).days + 1)//7

# d1 = date(2022, 9, 1)
# d2 = date(2022, 10, 1)
# for s in daterange(d1, d2):
#     print(s)


def week_number(year, month, day):
    '''Returns week number'''
    return date(year, month, day).isocalendar()[1]


def first_and_last_days(start_year, start_month, months):
    '''Returns the monday of the first week and the sunday of the last week of the desired time interval'''
    d = date(start_year, start_month, 1)
    d1 = d - timedelta(days=d.weekday())

    rel_end_month = start_month + months - 1
    d = date(start_year + rel_end_month//12, (rel_end_month)%12 + 1, 1)
    d2 = d + timedelta(days=6-d.weekday())
    return(d1, d2)


def daterange(start_date, end_date):
    '''Iterates on days between start_date and end_date'''
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


def get_month(n):
    if n == 1: return('janvier')
    if n == 2: return('février')
    if n == 3: return('mars')
    if n == 4: return('avril')
    if n == 5: return('mai')
    if n == 6: return('juin')
    if n == 7: return('juillet')
    if n == 8: return('août')
    if n == 9: return('septembre')
    if n == 10: return('octobre')
    if n == 11: return('novembre')
    if n == 12: return('décembre')


###############################################################################
# LaTeX generating functions


def generate_pages_monthly(year, month):
    fd, nod = calendar.monthrange(year, month)
    
    filling = (str(month), get_month(month), str(year))
    days = tuple('{}'.format(n-fd+1) if nod >= n-fd+1 > 0 else ' ' for n in range(42))
    leftpage = tuple(v for i, v in enumerate(days) if i%7 < 3)
    rightpage = tuple(v for i, v in enumerate(days) if i%7 >= 3)
    
    filling = filling + leftpage + rightpage

    a = '''
{
    \\newpage%%
    \\noindent%%
    \\begin{tabular}{C C C C}%%
        {\\LARGE \\bf %s}\\quad
        \\begin{minipage}{4em}
            \\scriptsize \\vspace{-8pt}%s \\\\[-2pt] \\scriptsize %s
        \\end{minipage}
        & \\bf LUN & \\bf MAR & \\bf MER \\tabularnewline%%
    \\end{tabular}%%

    \\noindent\\begin{tabular}{| B | B | B | B |}%%
    \\hline%%
    & %s & %s & %s \\tabularnewline \\cline{2-4}\\noalign{\\vskip\\arrayrulewidth}
    & %s & %s & %s \\tabularnewline \\cline{2-4}\\noalign{\\vskip\\arrayrulewidth}
    & %s & %s & %s \\tabularnewline \\cline{2-4}\\noalign{\\vskip\\arrayrulewidth}
    & %s & %s & %s \\tabularnewline \\cline{2-4}\\noalign{\\vskip\\arrayrulewidth}
    & %s & %s & %s \\tabularnewline \\cline{2-4}\\noalign{\\vskip\\arrayrulewidth}
    & %s & %s & %s \\tabularnewline \\hline
    \\end{tabular}%%

    \\newpage%%
    \\noindent\\begin{tabular}{C C C C}
        \\bf JEU & \\bf VEN & \\bf SAM & \\bf DIM \\tabularnewline
    \\end{tabular}%%

    \\noindent\\begin{tabular}{| B | B | B | B |}%%
    \\hline%%
    %s & %s & %s & %s \\tabularnewline \\hline
    %s & %s & %s & %s \\tabularnewline \\hline
    %s & %s & %s & %s \\tabularnewline \\hline
    %s & %s & %s & %s \\tabularnewline \\hline
    %s & %s & %s & %s \\tabularnewline \\hline
    %s & %s & %s & %s \\tabularnewline \\hline
    \\end{tabular}%%
}
''' % filling
    return(a)


def generate_pages_weekly(year, month, day):
    start_date = date(year, month, day)
    # generate the days first
    days = tuple(d.day for d in daterange(start_date, start_date+timedelta(days=14)))
    # get the week and month numbers
    week_num = (str(start_date.isocalendar()[1]), str((start_date+timedelta(days=7)).isocalendar()[1]))
    d1 = start_date + timedelta(days=6)
    d2 = start_date + timedelta(days=7)
    d3 = start_date + timedelta(days=13)
    if start_date.month == d1.month:
        month_num = (str(month),)
    else:
        month_num = (str(month)+" \\rule{.5pt}{13pt} "+str(d1.month),)
    if d2.month == d3.month:
        month_num += (str(d2.month),)
    else:
        month_num += (str(d2.month)+" \\rule{.5pt}{13pt} "+str(d3.month),)
    # now, determine the week number (= row number) within the month. This will
    # determine where to place boxes in the tiny month calendars. There are many
    # different possibilities, depending on the current month's length, and if
    # the two weeks overlap into the next month.
    week_row = (day + 5)//7
    fd, nod = calendar.monthrange(year, month)
    if day + 13 <= nod:         # no overlap
        boxed_rows_left = 2
        boxed_rows_right = 0
    elif day + 6 == nod:        # month ends when week ends
        boxed_rows_left = 1
        boxed_rows_right = 1
    elif day + 6 > nod:         # month ends within first week
        boxed_rows_left = 1
        boxed_rows_right = 2
    else:                       # month ends within second week
        boxed_rows_left = 2
        boxed_rows_right = 1

    default_line = '''        & %s & %s & %s & %s & %s & %s & %s \\tabularnewline\n'''
    left_cal = default_line*week_row
    if boxed_rows_left == 2:
        left_cal += '''        \\cline{2-8} & \\multicolumn{1}{|c}{%s} & %s & %s & %s & %s & %s & \\multicolumn{1}{c|}{%s} \\tabularnewline\n        & \\multicolumn{1}{|c}{%s} & %s & %s & %s & %s & %s & \\multicolumn{1}{c|}{%s} \\tabularnewline \\cline{2-8}\n''' + default_line*(4-week_row)
    else:
        left_cal += '''        \\cline{2-8} & \\multicolumn{1}{|c}{%s} & %s & %s & %s & %s & %s & \\multicolumn{1}{c|}{%s} \\tabularnewline \\cline{2-8}\n''' + default_line*(5-week_row)
    if boxed_rows_right == 0:
        right_cal = default_line*6
    elif boxed_rows_right == 1:
        right_cal = '''        \\cline{2-8} & \\multicolumn{1}{|c}{%s} & %s & %s & %s & %s & %s & \\multicolumn{1}{c|}{%s} \\tabularnewline \\cline{2-8}\n''' + default_line*5
    elif boxed_rows_right == 2:
        right_cal = '''        \\cline{2-8} & \\multicolumn{1}{|c}{%s} & %s & %s & %s & %s & %s & \\multicolumn{1}{c|}{%s} \\tabularnewline\n        & \\multicolumn{1}{|c}{%s} & %s & %s & %s & %s & %s & \\multicolumn{1}{c|}{%s} \\tabularnewline \\cline{2-8}\n''' + default_line*4

    # We have all the elements, now put them together
    a_left = '''
{
    \\newpage
    \\noindent\\begin{tabular}{S}

    \\begin{minipage}{13em}
        \\vspace{50pt}
        {\\bf {\\LARGE %s}}\\quad
        \\begin{minipage}{6em}
          \\scriptsize \\vspace{-7pt}{\\bf semaine %s} \\\\[-2pt] \\scriptsize %s %s
        \\end{minipage}
    \\end{minipage}\\hfill
    {\\scriptsize
        \\begin{tabular}{MMMMMMMM}
        \\y \\multicolumn{1}{c|}{\\bf  %s} &\\bf L &\\bf  M &\\bf  M &\\bf  J &\\bf  V &\\bf  S &\\bf  D \\tabularnewline \\g
''' + left_cal + '''\\end{tabular}
    }
    \\tabularnewline \\y\\hline\\g
    \\vspace{20pt}\\begin{tabular}{c}\\y
        \\bf \\large %s\\tabularnewline \\hline
        \\bf \\scriptsize LUN
    \\end{tabular}
    \\tabularnewline \\hline
    \\vspace{20pt}\\begin{tabular}{c}\\y
        \\bf \\large %s\\tabularnewline \\hline
        \\bf \\scriptsize MAR
    \\end{tabular}
    \\tabularnewline \\hline
    \\vspace{20pt}\\begin{tabular}{c}\\y
        \\bf \\large %s\\tabularnewline \\hline
        \\bf \\scriptsize MER
    \\end{tabular}
    \\tabularnewline \\hline
    \\vspace{20pt}\\begin{tabular}{c}\\y
        \\bf \\large %s\\tabularnewline \\hline
        \\bf \\scriptsize JEU
    \\end{tabular}
    \\tabularnewline \\hline
    \\vspace{20pt}\\begin{tabular}{c}\\y
        \\bf \\large %s\\tabularnewline \\hline
        \\bf \\scriptsize VEN
    \\end{tabular}
    \\tabularnewline \\hline
    \\vspace{20pt}\\begin{tabular}{c}\\y
        \\bf \\large %s\\tabularnewline \\hline
        \\bf \\scriptsize SAM
    \\end{tabular}
    \\tabularnewline \\hline
    \\vspace{20pt}\\begin{tabular}{c}\\y
        \\bf \\large %s\\tabularnewline \\hline
        \\bf \\scriptsize DIM
    \\end{tabular}
    \\end{tabular}
}
'''
    a_right = '''
{
    \\newpage
    \\noindent\\begin{tabular}{T}
    {\\scriptsize
    \\begin{tabular}{MMMMMMMM}
    \\y \\multicolumn{1}{c|}{\\bf  %s} &\\bf L &\\bf  M &\\bf  M &\\bf  J &\\bf  V &\\bf  S &\\bf  D \\tabularnewline \\g
''' + right_cal + '''\\end{tabular}
    }
    \\hfill
    \\begin{minipage}{13em}
        \\raggedleft\\vspace{50pt}
        \\begin{minipage}{6em}\\raggedleft
            \\scriptsize \\vspace{-7pt}{\\bf semaine %s} \\\\[-2pt] \\scriptsize %s %s
        \\end{minipage}
        \\quad {\\bf{\\LARGE %s}}
    \\end{minipage}
    \\tabularnewline \\y\\hline\\g
    \\vspace{20pt}\\begin{tabular}{c}\\y
        \\bf \\large %s\\tabularnewline \\hline
        \\bf \\scriptsize LUN
    \\end{tabular}
    \\tabularnewline \\hline
    \\vspace{20pt}\\begin{tabular}{c}\\y
        \\bf \\large %s\\tabularnewline \\hline
        \\bf \\scriptsize MAR
    \\end{tabular}
    \\tabularnewline \\hline
    \\vspace{20pt}\\begin{tabular}{c}\\y
        \\bf \\large %s\\tabularnewline \\hline
        \\bf \\scriptsize MER
    \\end{tabular}
    \\tabularnewline \\hline
    \\vspace{20pt}\\begin{tabular}{c}\\y
        \\bf \\large %s\\tabularnewline \\hline
        \\bf \\scriptsize JEU
    \\end{tabular}
    \\tabularnewline \\hline
    \\vspace{20pt}\\begin{tabular}{c}\\y
        \\bf \\large %s\\tabularnewline \\hline
        \\bf \\scriptsize VEN
    \\end{tabular}
    \\tabularnewline \\hline
    \\vspace{20pt}\\begin{tabular}{c}\\y
        \\bf \\large %s\\tabularnewline \\hline
        \\bf \\scriptsize SAM
    \\end{tabular}
    \\tabularnewline \\hline
    \\vspace{20pt}\\begin{tabular}{c}\\y
        \\bf \\large %s\\tabularnewline \\hline
        \\bf \\scriptsize DIM
    \\end{tabular}
    \\end{tabular}
}
'''

    left_fill = (month_num[0], week_num[0], get_month(month), str(year), str(month)) + tuple('{}'.format(n-fd+1) if nod >= n-fd+1 > 0 else ' ' for n in range(42)) + tuple(str(d) for d in days[:7])

    fd2, nod2 = calendar.monthrange(year, month%12+1)
    right_fill = (str(month%12 + 1),) + tuple('{}'.format(n-fd2+1) if nod2 >= n-fd2+1 > 0 else ' ' for n in range(42)) + (week_num[1], get_month(d2.month), str(d2.year), month_num[1]) + tuple(str(d) for d in days[7:])

    return(a_left%left_fill + a_right%right_fill)


###############################################################################
###############################################################################
# Write the output file

f = open(output, 'w')

f.write('''
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\\documentclass[a5paper, twoside, 12pt]{article}
\\usepackage[utf8]{inputenc}
\\usepackage[T1]{fontenc}

%%------------------------------------------------------------------------------
%% Packages

\\usepackage[top=.5cm, bottom=.5cm, inner=.4cm, outer=.6cm]{geometry}
\\usepackage{graphicx}
\\usepackage{array}
\\usepackage[table]{xcolor}

\\usepackage[french]{babel} % load last

%%------------------------------------------------------------------------------
%% Custom environments/macros

\\newcolumntype{C}{%
  >{\\centering\\vphantom{\\Huge 8}}
  m{3.45cm}%
}
\\newcolumntype{B}{%
  >{\\vbox to 2.54cm\\bgroup\\raggedright\\vspace{3pt}\\hspace{4pt}}%
  p{3.43cm}% width is adjusted to span 13.85cm with 4 columns + 5 separators
  <{\\vfill\\egroup}
}
\\newcolumntype{S}{%
  >{\\vbox to 1.85cm\\bgroup\\raggedright}
  m{13.79cm}%
  <{\\vfill\\egroup}
}
\\newcolumntype{T}{%
  >{\\vbox to 1.85cm\\bgroup\\raggedleft}
  m{13.79cm}%
  <{\\vfill\\egroup}
}
\\newcolumntype{M}{%
  >{\\centering}%
  p{14pt}
}
\\newcolumntype{A}{%
  p{3.29cm}
}  
\\newcolumntype{D}{%
  >{\\vbox to -.06cm\\bgroup\\raggedright}
  m{3.29cm}
  <{\\vfill\\egroup}
}  


\\setlength{\\tabcolsep}{0cm}

\\newcommand{\\y}{\\arrayrulecolor{black}}
\\newcommand{\\g}{\\arrayrulecolor{gray}}
\\newcommand{\\wg}{\\arrayrulecolor{lightgray}}

\\renewcommand{\\familydefault}{\\sfdefault}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

{
\\vspace*{\\stretch{2}}
\\centering\\Huge{Agenda}
\\vspace*{\\stretch{2}}
}
''')

if print_week_planner:
    f.write('''
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Week planner
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\\global\\arrayrulewidth=.2pt

{
  \\newpage
  \\y\\noindent%
  \\begin{tabular}{A>{\\raggedleft\\scriptsize}p{.6cm}<{\\hphantom{.}}AAA}
    && \\bf \\centering LUN & \\bf \\centering MAR & \\bf \\centering MER
  \\end{tabular}
  
  \\noindent\\begin{tabular}{D>{\\raggedleft\\scriptsize}p{.6cm}<{\\hphantom{.}}|D|D|D|}
  \\cline{3-5}
  &7h & & & \\tabularnewline\\wg\\cline{3-5}
  && & & \\tabularnewline\\y\\cline{3-5} 
  &8h & & & \\tabularnewline\\wg\\cline{3-5}
  && & & \\tabularnewline\\y\\cline{3-5}     
  &9h & & & \\tabularnewline\\wg\\cline{3-5} 
  && & & \\tabularnewline\\y\\cline{3-5}     
  &10h & & & \\tabularnewline\\wg\\cline{3-5}
  && & & \\tabularnewline\\y\\cline{3-5}     
  &11h& & & \\tabularnewline\\wg\\cline{3-5} 
  && & & \\tabularnewline\\y\\cline{3-5}     
  &12h & & & \\tabularnewline\\wg\\cline{3-5}
  && & & \\tabularnewline\\y\\cline{3-5}     
  &13h & & & \\tabularnewline\\wg\\cline{3-5}
  && & & \\tabularnewline\\y\\cline{3-5}     
  &14h & & & \\tabularnewline\\wg\\cline{3-5}
  && & & \\tabularnewline\\y\\cline{3-5}     
  &15h & & & \\tabularnewline\\wg\\cline{3-5}
  && & & \\tabularnewline\\y\\cline{3-5}     
  &16h & & & \\tabularnewline\\wg\\cline{3-5}
  && & & \\tabularnewline\\y\\cline{3-5}     
  &17h & & & \\tabularnewline\\wg\\cline{3-5}
  && & & \\tabularnewline\\y\\cline{3-5}     
  &18h & & & \\tabularnewline\\wg\\cline{3-5}    
  && & & \\tabularnewline\\y\\cline{3-5}     
  &19h & & & \\tabularnewline\\wg\\cline{3-5}
  && & & \\tabularnewline\\y\\cline{3-5}     
  &20h & & & \\tabularnewline\\wg\\cline{3-5}
  && & & \\tabularnewline\\y\\cline{3-5}     
  &21h & & & \\tabularnewline\\wg\\cline{3-5}
  && & & \\tabularnewline\\y\\cline{3-5}     
  &22h & & & \\tabularnewline\\wg\\cline{3-5}
  && & & \\tabularnewline\\y\\cline{3-5}     
  \\end{tabular}
}

{
  \\newpage
    \\newpage
  \\y\\noindent%
  \\begin{tabular}{AAAA>{\\raggedleft\\scriptsize}p{.6cm}<{\\hphantom{.}}}
    \\bf \\centering JEU & \\bf \\centering VEN & \\bf \\centering SAM & \\bf\\centering DIM &
  \\end{tabular}
  
  \\noindent\\begin{tabular}{|D|D|D|D|>{\\raggedleft\\scriptsize}p{.6cm}<{\\hphantom{.}}}
  \\cline{1-4}
    && & & 7h \\tabularnewline\\wg
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 8h \\tabularnewline\\wg\\cline{1-4}
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 9h \\tabularnewline\\wg\\cline{1-4}
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 10h \\tabularnewline\\wg\\cline{1-4}
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 11h \\tabularnewline\\wg\\cline{1-4}
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 12h \\tabularnewline\\wg\\cline{1-4}
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 13h \\tabularnewline\\wg\\cline{1-4}
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 14h \\tabularnewline\\wg\\cline{1-4}
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 15h \\tabularnewline\\wg\\cline{1-4}
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 16h\\tabularnewline\\wg\\cline{1-4}
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 17h \\tabularnewline\\wg\\cline{1-4}
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 18h \\tabularnewline\\wg\\cline{1-4}    
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 19h \\tabularnewline\\wg\\cline{1-4}
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 20h \\tabularnewline\\wg\\cline{1-4}
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 21h \\tabularnewline\\wg\\cline{1-4}
    && & & \\tabularnewline\\y\\cline{1-4}
    && & & 22h \\tabularnewline\\wg\\cline{1-4}
    && & & \\tabularnewline\\y\\cline{1-4}
  \\end{tabular}
}

\\global\\arrayrulewidth=.4pt
''')

if print_monthly_agenda:
    month_list = ((start_year+(n+start_month-1)//12, (start_month+n-1)%12+1) for n in range(months))
    f.write('''
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Monthly agenda
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
''')
    for year, month in month_list:
        f.write(generate_pages_monthly(year, month))

if print_weekly_agenda:
    d1, d2 = first_and_last_days(start_year, start_month, months)
    d = d1 + timedelta(days=14)
    weeks_list = [d1]
    while d < d2:
        weeks_list.append(d)
        d += timedelta(days=14)

    f.write('''%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Weekly agenda
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
''')
    for w in weeks_list:
        f.write(generate_pages_weekly(w.year, w.month, w.day))

f.write('''
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
''')
f.close()
